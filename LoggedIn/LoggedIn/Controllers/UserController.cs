﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using LoggedIn.Models;
using LoggedIn.Data;

namespace LoggedIn.Controllers
{
    public class UserController : Controller
    {
        private readonly UserContext _context;
        public const string ActiveUser = "Not Blank";

        public UserController(UserContext context)
        {
            _context = context;
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Registration([Bind("Id, Username, Password")] User user)
        {
            if (ModelState.IsValid)
            {
                _context.Add(user);
                await _context.SaveChangesAsync();
                return RedirectToAction("Login", "User");
            }

            return RedirectToAction("Home", "Error");
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> LoggingIn(string username, string password)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var SearchUser = await _context.Users.SingleOrDefaultAsync(check => check.Username == username && check.Password == password);
                    if (SearchUser != null)
                    {
                        HttpContext.Session.SetString(ActiveUser, username);
                        return RedirectToAction("Completed", "Home");
                    }
                }
                catch
                { }
            }

            return RedirectToAction("Error", "Home");
        }

        public IActionResult Login()
        {
            return View();
        }

        public IActionResult Register()
        {
            return View();
        }
    }
}
