﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggedIn.Data
{
    public class DbInitialize
    {
        public static void Initialize(UserContext context)
        {
            context.Database.EnsureCreated();
        }
    }
}
